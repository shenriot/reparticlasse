// Fonction pour mélanger un tableau
function shuffleArray(array) {
    for (let i = array.length - 1; i > 0; i--) {
        const j = Math.floor(Math.random() * (i + 1));
        [array[i], array[j]] = [array[j], array[i]];
    }
    return array;
}

// Fonction pour équilibrer les groupes par genre
function balanceByGender(students, groupCount) {
    const males = students.filter(student => student.gender === 'm');
    const females = students.filter(student => student.gender === 'f');

    // Calcul du nombre d'étudiants par groupe
    const totalStudents = students.length;
    const baseGroupSize = Math.floor(totalStudents / groupCount);
    const remainder = totalStudents % groupCount;

    // Initialiser les groupes
    const groups = Array.from({ length: groupCount }, (_, i) => ({
        students: [],
        maxSize: baseGroupSize + (i < remainder ? 1 : 0),
    }));

    // Répartir hommes et femmes dans les groupes en alternance
    let maleIndex = 0, femaleIndex = 0;

    groups.forEach(group => {
        while (group.students.length < group.maxSize) {
            // Ajouter un homme si possible
            if (maleIndex < males.length) {
                group.students.push(males[maleIndex]);
                maleIndex++;
            }

            // Ajouter une femme si possible
            if (femaleIndex < females.length && group.students.length < group.maxSize) {
                group.students.push(females[femaleIndex]);
                femaleIndex++;
            }
        }
    });

    return groups.map(group => group.students);
}



// Convertir les niveaux en valeurs numériques
function convertLevelToValue(level) {
    switch(level) {
        case '--':
            return -2;
        case '-':
            return -1;
        case '+':
            return 1;
        case '++':
            return 2;
        default:
            return 0;
    }
}

function groupByLevelHeterogeneous(students, groupCount) {
    // Trier les élèves par niveau décroissant
    students.sort((a, b) => b.levelValue - a.levelValue);

    const totalStudents = students.length;

    // Calculer la taille de base et le reste
    const baseGroupSize = Math.floor(totalStudents / groupCount);
    const remainder = totalStudents % groupCount;

    // Créer un tableau des tailles de groupes
    const groupSizes = Array(groupCount).fill(baseGroupSize);
    for (let i = 0; i < remainder; i++) {
        groupSizes[i] += 1;
    }

    // Initialiser les groupes avec leur taille maximale
    const groups = groupSizes.map(size => ({
        students: [],
        totalLevelValue: 0,
        maxSize: size
    }));

    // Répartir les élèves pour équilibrer les scores totaux et les tailles des groupes
    students.forEach(student => {
        // Trouver les groupes qui ne sont pas pleins
        const availableGroups = groups.filter(group => group.students.length < group.maxSize);

        // Trouver le groupe avec le score total le plus bas parmi les groupes disponibles
        let minGroup = availableGroups.reduce((prev, curr) => {
            return curr.totalLevelValue < prev.totalLevelValue ? curr : prev;
        }, availableGroups[0]);

        // Attribuer l'élève au groupe
        minGroup.students.push(student);
        minGroup.totalLevelValue += student.levelValue;
    });

    // Retourner les groupes (listes d'élèves)
    return groups.map(group => group.students);
}


function groupByLevelHomogeneous(students, groupCount) {
    // Regrouper les élèves par niveau
    const levelGroups = {};
    students.forEach(student => {
        const level = student.levelValue;
        if (!levelGroups[level]) {
            levelGroups[level] = [];
        }
        levelGroups[level].push(student);
    });

    // Calculer les tailles des groupes
    const totalStudents = students.length;
    const baseGroupSize = Math.floor(totalStudents / groupCount);
    const remainder = totalStudents % groupCount;

    // Créer les groupes avec leur taille maximale
    const groups = [];
    for (let i = 0; i < groupCount; i++) {
        groups.push({
            students: [],
            levels: [],
            maxSize: baseGroupSize + (i < remainder ? 1 : 0)
        });
    }

    // Définir les niveaux possibles
    const levels = Object.keys(levelGroups).map(Number).sort((a, b) => b - a);

    // Fonction pour trouver des groupes compatibles
    function findCompatibleGroups(level, maxLevelDifference) {
        return groups.filter(group =>
            group.students.length < group.maxSize &&
            (group.levels.length === 0 ||
                group.levels.some(lvl => Math.abs(lvl - level) <= maxLevelDifference))
        );
    }

    // Assigner les élèves
    levels.forEach(level => {
        const studentsOfLevel = levelGroups[level];

        // Priorité 1 : Essayer d'assigner à des groupes du même niveau
        let unassignedStudents = [...studentsOfLevel];
        unassignedStudents.forEach(student => {
            const sameLevelGroups = groups.filter(group =>
                group.students.length < group.maxSize && group.levels.includes(level)
            );
            if (sameLevelGroups.length > 0) {
                sameLevelGroups[0].students.push(student);
                unassignedStudents = unassignedStudents.filter(s => s !== student);
            }
        });

        // Priorité 2 : Essayer d'assigner à des groupes avec niveaux adjacents (différence de niveau de 1)
        unassignedStudents.forEach(student => {
            const adjacentLevelGroups = findCompatibleGroups(level, 1);
            if (adjacentLevelGroups.length > 0) {
                const group = adjacentLevelGroups[0];
                group.students.push(student);
                if (!group.levels.includes(level)) {
                    group.levels.push(level);
                }
            } else {
                // Priorité 3 : Essayer d'assigner à des groupes avec une différence de niveau de 2
                const distantLevelGroups = findCompatibleGroups(level, 2);
                if (distantLevelGroups.length > 0) {
                    const group = distantLevelGroups[0];
                    group.students.push(student);
                    if (!group.levels.includes(level)) {
                        group.levels.push(level);
                    }
                } else {
                    // Priorité 4 : Assigner à n'importe quel groupe ayant de la place
                    const availableGroups = groups.filter(group => group.students.length < group.maxSize);
                    if (availableGroups.length > 0) {
                        const group = availableGroups[0];
                        group.students.push(student);
                        if (!group.levels.includes(level)) {
                            group.levels.push(level);
                        }
                    } else {
                        // Tous les groupes sont pleins (ne devrait pas arriver)
                        console.warn(`Impossible d'assigner l'élève ${student.name}`);
                    }
                }
            }
        });
    });

    // Retourner les groupes
    return groups.map(group => group.students);
}




// Fonction auxiliaire pour calculer la différence minimale de niveau entre un groupe et un niveau donné
function groupLevelDifference(groupLevels, level) {
    if (groupLevels.length === 0) return Infinity;
    return Math.min(...groupLevels.map(lvl => Math.abs(lvl - level)));
}



// Fonction pour mélanger un tableau (existe déjà)
function shuffleArray(array) {
    for (let i = array.length - 1; i > 0; i--) {
        const j = Math.floor(Math.random() * (i + 1));
        [array[i], array[j]] = [array[j], array[i]];
    }
    return array;
}







// Variable pour suivre l'état du scoring activé/désactivé (désactivé par défaut)
let isScoringEnabled = false;

// Fonction pour afficher les groupes avec les avatars et la gestion des scores
function displayGroups(groups, avatarStyle, groupsContainer) {
    // Mélanger les groupes avant de les afficher
    const shuffledGroups = shuffleArray(groups);

    shuffledGroups.forEach((group, index) => {
        const groupDiv = document.createElement('div');
        groupDiv.classList.add('group', 'bg-gray-100', 'p-3', 'rounded-lg', 'shadow-lg', 'max-w-xs', 'mx-auto', 'm-4');

        const groupTitle = document.createElement('h2');
        groupTitle.classList.add('text-base', 'font-bold', 'mb-2');
        groupTitle.textContent = `Groupe ${index + 1}`;

        // Ajouter le score cumulé pour le groupe sous forme de badge discret
        const groupScoreLabel = document.createElement('span');
        groupScoreLabel.classList.add('group-score', 'text-white', 'rounded-full', 'px-2', 'py-1', 'text-xs', 'ml-2');
        groupScoreLabel.textContent = group.totalScore || 0; // Afficher le score total du groupe
        groupScoreLabel.id = `group-score-${index}`; // ID unique pour chaque groupe

        // Appliquer une couleur en fonction du score du groupe
        if (group.totalScore >= 0) {
            groupScoreLabel.classList.add('bg-green-500'); // Vert pour les scores positifs
        } else {
            groupScoreLabel.classList.add('bg-red-500'); // Rouge pour les scores négatifs
        }

        // Cacher les scores au démarrage si le scoring est désactivé
        groupScoreLabel.style.display = isScoringEnabled ? 'inline-block' : 'none';

        // Ajouter le badge du score au titre du groupe
        groupTitle.appendChild(groupScoreLabel);

        const groupMembers = document.createElement('div');
        groupMembers.classList.add('grid', 'grid-cols-2', 'gap-2');

        // Initialiser le score de groupe
        group.index = index; // Enregistrer l'indice du groupe

        // Score individuel et événements pour chaque avatar
        group.forEach((student, studentIndex) => {
            const studentDiv = document.createElement('div');
            studentDiv.classList.add('student', 'flex', 'flex-col', 'items-center', 'm-1', 'relative');

            const seed = Math.random().toString(36).substring(7); // Graine aléatoire pour l'avatar
            const avatarImg = document.createElement('img');
            avatarImg.src = `https://api.dicebear.com/6.x/${avatarStyle}/svg?seed=${student.avatarSeed || Math.random().toString(36).substring(7)}`;
            avatarImg.alt = `Avatar de ${student.name}`;
            avatarImg.classList.add('w-12', 'h-12', 'rounded-full', 'border-2', 'border-gray-300', 'mb-1');

            const nameLabel = document.createElement('p');
            nameLabel.textContent = `${student.name}`;
            nameLabel.classList.add('text-xs', 'font-medium', 'text-center');

            // Score individuel sous forme de badge au-dessus de l'avatar
            const studentScoreLabel = document.createElement('span');
            studentScoreLabel.classList.add('student-score', student.score >= 0 ? 'bg-green-500' : 'bg-red-500', 'text-white', 'rounded-full', 'px-2', 'py-1', 'text-xs', 'absolute', '-top-3', 'right-3');
            studentScoreLabel.textContent = student.score || 0; // Utiliser le score existant ou 0 par défaut
            studentScoreLabel.id = `student-score-${index}-${studentIndex}`;

            // Cacher les scores des étudiants au démarrage si le scoring est désactivé
            studentScoreLabel.style.display = isScoringEnabled ? 'inline-block' : 'none';

            // Stocker le score de chaque étudiant dans l'objet "student"
            student.score = student.score || 0;

            studentDiv.appendChild(avatarImg);
            studentDiv.appendChild(nameLabel);
            studentDiv.appendChild(studentScoreLabel); // Ajouter le badge score au div de l'étudiant
            groupMembers.appendChild(studentDiv);

            // Gestion des clics et double-clics seulement si le scoring est activé
            let clickTimeout;
            avatarImg.addEventListener('click', () => {
                if (isScoringEnabled) {
                    if (clickTimeout) {
                        clearTimeout(clickTimeout);
                        clickTimeout = null;
                        // Double-clic - retirer un point
                        updateStudentScore(group, student, studentScoreLabel, -1);
                        updateGroupScore(group, groupScoreLabel); // Mettre à jour le score du groupe
                    } else {
                        // Si aucun double clic n'est détecté, on attend un peu
                        clickTimeout = setTimeout(() => {
                            updateStudentScore(group, student, studentScoreLabel, 1); // Ajouter un point
                            updateGroupScore(group, groupScoreLabel); // Mettre à jour le score du groupe
                            clickTimeout = null;
                        }, 300); // Délai avant d'enregistrer un simple clic
                    }
                }
            });
        });

        groupDiv.appendChild(groupTitle);
        groupDiv.appendChild(groupMembers);
        groupsContainer.appendChild(groupDiv);
    });
}

// Fonction pour mettre à jour le score total d'un groupe et changer la couleur en fonction du score
function updateGroupScore(group, groupScoreLabel) {
    const totalScore = group.reduce((total, student) => total + student.score, 0);
    groupScoreLabel.textContent = totalScore;

    // Changer la couleur du score en fonction du score total
    groupScoreLabel.classList.remove('bg-green-500', 'bg-red-500');
    if (totalScore >= 0) {
        groupScoreLabel.classList.add('bg-green-500');
    } else {
        groupScoreLabel.classList.add('bg-red-500');
    }
}



// Fonction pour mettre à jour le score d'un étudiant
function updateStudentScore(group, student, studentScoreLabel, scoreChange) {
    // Mettre à jour le score de l'étudiant
    student.score += scoreChange;
    studentScoreLabel.textContent = student.score;

    // Mettre à jour le score total du groupe
    group.totalScore = group.reduce((total, student) => total + student.score, 0);

    // Mettre à jour l'affichage du score de groupe
    const groupScoreLabel = document.getElementById(`group-score-${group.index}`);
    groupScoreLabel.textContent = group.totalScore;

    // Changer la couleur du score en fonction de la valeur (positif/negatif)
    if (student.score >= 0) {
        studentScoreLabel.classList.remove('bg-red-500');
        studentScoreLabel.classList.add('bg-green-500');
    } else {
        studentScoreLabel.classList.remove('bg-green-500');
        studentScoreLabel.classList.add('bg-red-500');
    }
}


// Fonction pour mettre à jour la couleur du score en fonction de la valeur
function updateScoreColor(label, score) {
    if (score <= 0) {
        label.classList.remove('bg-green-500');
        label.classList.add('bg-red-500');
    } else {
        label.classList.remove('bg-red-500');
        label.classList.add('bg-green-500');
    }
}







// Fonction pour reconnaître la structure des groupes déjà composés
function parsePredefinedGroups(inputText) {
    // Diviser les groupes par le symbole "/"
    const groupStrings = inputText.split('/');
    const groups = groupStrings.map(groupString => {
        // Diviser les élèves dans chaque groupe par des virgules
        const studentStrings = groupString.split(',').map(name => name.trim());
        const students = studentStrings.map(student => {
            const parts = student.split(' ');
            return {
                name: parts[0],
                gender: parts[1], // "f" pour femme, "m" pour homme
                level: parts[2] || null, // "--", "-", "+", "++"
                score: 0 // Initialiser le score à 0
            };
        });
        return students; // Retourner les étudiants du groupe
    });
    return groups;
}






// Activer/désactiver l'option de sélection homogène/hétérogène
document.getElementById('levelOption').addEventListener('change', (e) => {
    const levelBalanceOptions = document.getElementById('levelBalanceOptions');
    if (e.target.checked) {
        levelBalanceOptions.classList.remove('hidden');
    } else {
        levelBalanceOptions.classList.add('hidden');
    }
});


let groups = [];  // Variable globale pour stocker les groupes

document.getElementById('generateGroupsButton').addEventListener('click', () => {
    const inputText = document.getElementById('nameInput').value;
    const groupsContainer = document.getElementById('groupsContainer');
    const avatarStyle = document.getElementById('avatarStyle').value;
    
    groupsContainer.innerHTML = ''; // Vider les anciens groupes

    // Vérifier si l'utilisateur a défini des groupes avec "/"
    if (inputText.includes('/')) {
        // Créer les groupes à partir de la structure déjà composée
        groups = parsePredefinedGroups(inputText);
    } else {
        // Si aucun groupe prédéfini, suivre la logique de répartition normale
        const groupCount = parseInt(document.getElementById('groupCount').value);
        const mixityOption = document.getElementById('mixityOption').checked;
        const levelOption = document.getElementById('levelOption').checked;
        
        let students = inputText.split(',').map(name => {
            const parts = name.trim().split(' ');
            return {
                name: parts[0],
                gender: parts[1], // "f" pour femme, "m" pour homme
                level: parts[2] || null // "--", "-", "+", "++"
            };
        });

        // Convertir les niveaux en valeurs numériques
        students.forEach(student => {
            student.levelValue = convertLevelToValue(student.level);
        });

        // Mélanger les étudiants aléatoirement avant de les répartir
        students = shuffleArray(students);

        // Si l'option de mixité est activée
        if (mixityOption) {
            groups = balanceByGender(students, groupCount);
        } else {
            // Répartition aléatoire sans considération de niveau ou de genre
            groups = [];
            for (let i = 0; i < groupCount; i++) {
                groups.push([]);
            }
            students.forEach((student, index) => {
                const groupIndex = index % groupCount;
                groups[groupIndex].push(student);
            });
        }
    }

    // Afficher les groupes générés ou pré-composés
    displayGroups(groups, avatarStyle, groupsContainer);
});








// Fonction pour importer les groupes et les scores à partir d'un fichier CSV
function importGroupsAndScoresFromCSV(file) {
    const reader = new FileReader();
    
    reader.onload = function (e) {
        const csv = e.target.result;
        const rows = csv.split('\n').slice(1); // Ignorer la première ligne (en-tête)
        
        const importedGroups = [];
        rows.forEach(row => {
            if (row.trim()) {
                const [group, name, score] = row.split(',');
                const groupIndex = parseInt(group.replace('Groupe ', '').trim()) - 1;
                
                // Si le groupe n'existe pas, le créer
                if (!importedGroups[groupIndex]) {
                    importedGroups[groupIndex] = [];
                }
                
                // Ajouter l'élève avec son nom et son score importé
                importedGroups[groupIndex].push({
                    name: name.trim(),
                    score: parseInt(score) || 0, // Score avec conversion en entier
                    avatarSeed: Math.random().toString(36).substring(7) // Génére une graine d'avatar aléatoire
                });
            }
        });
        
        // Afficher les groupes importés avec les scores
        const groupsContainer = document.getElementById('groupsContainer');
        const avatarStyle = document.getElementById('avatarStyle').value; // Récupérer le style d'avatar sélectionné
        groupsContainer.innerHTML = ''; // Vider l'affichage précédent
        displayGroups(importedGroups, avatarStyle, groupsContainer); // Reconstituer les groupes avec les avatars

        // Mettre à jour la variable globale des groupes
        groups = importedGroups;
    };
    
    reader.readAsText(file);
}












// Fonction pour exporter les groupes et les scores au format CSV
function exportGroupsWithScoresToCSV(groups) {
    let csvContent = "data:text/csv;charset=utf-8,";
    csvContent += "Groupe,Nom,Score\n"; // En-tête du fichier CSV

    // Parcourir les groupes et leurs membres pour générer le contenu CSV
    groups.forEach((group, groupIndex) => {
        group.forEach(student => {
            csvContent += `Groupe ${groupIndex + 1},${student.name},${student.score || 0}\n`; // Ajouter le score s'il existe
        });
    });

    // Créer un lien pour télécharger le fichier CSV
    const encodedUri = encodeURI(csvContent);
    const link = document.createElement("a");
    link.setAttribute("href", encodedUri);
    link.setAttribute("download", "groupes_repartis_scores.csv");
    document.body.appendChild(link); // Requis pour Firefox
    link.click();
    document.body.removeChild(link);
}

// Fonction pour importer les groupes et les scores à partir d'un fichier CSV
function importGroupsAndScoresFromCSV(file) {
    const reader = new FileReader();
    
    reader.onload = function (e) {
        const csv = e.target.result;
        const rows = csv.split('\n').slice(1); // Ignorer la première ligne (en-tête)
        
        const importedGroups = [];
        rows.forEach(row => {
            if (row.trim()) {
                const [group, name, score] = row.split(',');
                const groupIndex = parseInt(group.replace('Groupe ', '').trim()) - 1;
                
                // Si le groupe n'existe pas, le créer
                if (!importedGroups[groupIndex]) {
                    importedGroups[groupIndex] = [];
                }
                
                // Ajouter l'élève avec son nom et son score importé
                importedGroups[groupIndex].push({
                    name: name.trim(),
                    score: parseInt(score) || 0, // Score avec conversion en entier
                    avatarSeed: Math.random().toString(36).substring(7) // Génére une graine d'avatar aléatoire
                });
            }
        });

        // Recalculer les scores de groupes
        importedGroups.forEach(group => {
            group.totalScore = group.reduce((total, student) => total + student.score, 0);
        });
        
        // Afficher les groupes importés avec les scores
        const groupsContainer = document.getElementById('groupsContainer');
        const avatarStyle = document.getElementById('avatarStyle').value; // Récupérer le style d'avatar sélectionné
        groupsContainer.innerHTML = ''; // Vider l'affichage précédent
        displayGroups(importedGroups, avatarStyle, groupsContainer); // Reconstituer les groupes avec les avatars et scores

        // Mettre à jour la variable globale des groupes
        groups = importedGroups;
    };
    
    reader.readAsText(file);
}



// Gestion du fichier d'import
document.getElementById('importGroupsButton').addEventListener('change', function () {
    const file = this.files[0];
    if (file) {
        importGroupsAndScoresFromCSV(file);
    }
});

// Gestion du bouton d'exportation
document.getElementById('exportGroupsButton').addEventListener('click', () => {
    if (groups.length > 0) { // Vérifier qu'il y a des groupes générés
        exportGroupsWithScoresToCSV(groups); // Exporter avec les scores
    } else {
        alert('Aucun groupe généré pour l\'instant.');
    }
});






let isCreationZoneVisible = true; // La zone de création est visible par défaut

// Fonction pour activer/désactiver le scoring
document.getElementById('toggleScoringButton').addEventListener('click', () => {
    isScoringEnabled = !isScoringEnabled;

    // Changer l'icône et le texte du bouton en fonction de l'état du scoring
    const scoringIcon = document.getElementById('scoringIcon');
    const scoringText = document.getElementById('scoringText');
    if (isScoringEnabled) {
        scoringIcon.classList.replace('fa-chart-line', 'fa-stopwatch');
        scoringText.textContent = 'Désactiver le scoring';
    } else {
        scoringIcon.classList.replace('fa-stopwatch', 'fa-chart-line');
        scoringText.textContent = 'Activer Scoregroupe';
    }

    // Cacher/afficher les scores des étudiants et des groupes
    document.querySelectorAll('.student-score, .group-score').forEach(score => {
        score.style.display = isScoringEnabled ? 'inline-block' : 'none';
    });

    // Masquer la zone de création si le scoring est activé
    if (isScoringEnabled) {
        toggleCreationZone(false);
    }
});

// Fonction pour masquer/afficher la zone de création
function toggleCreationZone(forceHide = false) {
    const creationZone = document.getElementById('groupCreationZone');
    const toggleText = document.getElementById('toggleText');
    const toggleIcon = document.getElementById('toggleIcon');

    if (forceHide || isCreationZoneVisible) {
        creationZone.style.display = 'none';
        toggleText.textContent = 'Afficher la zone de création des groupes';
        toggleIcon.classList.replace('fa-chevron-down', 'fa-chevron-up');
        isCreationZoneVisible = false;
    } else {
        creationZone.style.display = 'block';
        toggleText.textContent = 'Cacher la zone de création des groupes';
        toggleIcon.classList.replace('fa-chevron-up', 'fa-chevron-down');
        isCreationZoneVisible = true;
    }
}

// Activer la fonction pour déplier/replier la zone de création
document.getElementById('toggleCreationZone').addEventListener('click', () => {
    toggleCreationZone();
});

// Fonction pour masquer la zone de création après import
document.getElementById('importGroupsButton').addEventListener('change', (e) => {
    const file = e.target.files[0];
    if (file) {
        importGroupsAndScoresFromCSV(file);
        toggleCreationZone(true); // Masquer la zone de création après l'import
    }
});

// Autres fonctions existantes (import, export, etc.)...





document.getElementById('helpButton').addEventListener('click', function() {
    introJs().setOptions({
        steps: [
            {
                intro: "Bienvenue sur Réparticlasse ! Ce guide vous aidera à comprendre comment utiliser l'application pour la répartition des groupes."
            },
            {
                element: document.querySelector('#toggleCreationZone'),
                intro: "Utilisez ce bouton pour afficher ou masquer la zone de création des groupes."
            },
            {
                element: document.querySelector('#nameInput'),
                intro: "Saisissez les prénoms des élèves ici. Vous pouvez également indiquer le genre et le niveau des élèves. Pour le genre utilisez la lettre f ou m et pour le niveau les symboles --, -, + et ++"
            },
            {
                element: document.querySelector('#maxGroupCount'),
                intro: "Indiquez ici le nombre maximum de groupes à générer."
            },
            {
                element: document.querySelector('#groupCount'),
                intro: "Utilisez cette barre pour définir combien de groupes vous souhaitez créer."
            },
            {
                element: document.querySelector('#mixityOption'),
                intro: "Cochez cette option si vous voulez assurer la mixité hommes/femmes dans les groupes."
            },
            {
                element: document.querySelector('#levelOption'),
                intro: "Cochez cette option pour équilibrer les groupes selon le niveau (--, -, +, ++)."
            },
            {
                element: document.querySelector('#avatarStyle'),
                intro: "Choisissez un style d'avatar pour représenter les élèves."
            },
            {
                element: document.querySelector('#generateGroupsButton'),
                intro: "Cliquez ici pour générer les groupes après avoir configuré les options."
            },
            {
                element: document.querySelector('#exportGroupsButton'),
                intro: "Cliquez ici pour sauvegarder la répartition des groupes."
            },
            {
                element: document.querySelector('label[for="importGroupsButton"]'),  // Cible le label visible
                intro: "Cliquez ici pour restaurer une répartition de groupes précédemment sauvegardée."
            },
            {
                element: document.querySelector('#toggleScoringButton'),
                intro: "Activez ou désactivez la gestion des points pour les groupes."
            },
            {
                element: document.querySelector('#groupsContainer'),
                intro: "Ici, vous verrez les groupes générés après avoir cliqué sur 'Générer les groupes'. Vous pouvez cliquer sur un avatar pour ajouter un point, et double-cliquer pour retirer un point."
            }
        ],
        nextLabel: 'Suivant',
        prevLabel: 'Précédent',
        doneLabel: 'Terminer'
    }).start();
});