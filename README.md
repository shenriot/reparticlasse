# Réparticlasse

**Réparticlasse** est une application web conçue pour faciliter la création et la gestion de groupes en classe. Elle permet aux enseignants de générer des groupes de manière aléatoire ou selon des critères tels que la mixité de genre et le niveau des élèves. De plus, l'application intègre un système de scoring basé sur le principe des « îlots bonifiés », permettant de suivre et d'évaluer l'investissement des élèves dans leurs groupes.

## Fonctionnalités

- **Création automatique de groupes** : Créez des groupes aléatoires ou en fonction de la mixité et du niveau des élèves.
- **Gestion des points** : Attribuez des points aux groupes ou aux élèves pour leurs interventions en utilisant le principe des « îlots bonifiés ».
- **Visualisation des groupes** : Affichez les groupes avec des avatars et suivez l'évolution des scores en temps réel.
- **Exportation et importation des scores** : Exportez et importez les scores et les compositions de groupes pour une gestion fluide des sessions.
- **Personnalisation des avatars** : Choisissez parmi différents styles d'avatars pour représenter visuellement les élèves.
- **Guide interactif** : Utilisez Intro.js pour un tutoriel guidé de l'application.

## Conformité RGPD et Protection des Données Personnelles

### Données Collectées

L'application collecte en local (sur la machine de l'utilisateur via son navigateur) les données suivantes :

- **Prénoms des élèves** : Informations personnelles identifiables directement.
- **Genre (`gender`)** : Donnée utilisée pour assurer la mixité dans les groupes.
- **Niveau (`level`)** : Indicateur académique des élèves.
- **Scores** : Points attribués aux élèves ou groupes basés sur leurs interventions.
- **Avatar Seed (`avatarSeed`)** : Graine utilisée pour générer des avatars personnalisés.

### Finalités du Traitement

Les données collectées sont utilisées pour :

- Générer et gérer des groupes d'élèves.
- Assurer la mixité de genre et l'équilibre des niveaux dans les groupes.
- Gérer et visualiser les scores individuels et de groupe.
- Générer des avatars personnalisés pour les élèves via l'API externe Dicebear.
- Importer et exporter des compositions de groupes et leurs scores au format CSV.


### Sécurité des Données

- **Transmission Sécurisée** : Toutes les communications entre le client et les services externes se font via HTTPS pour garantir la sécurité des données en transit.
- **Mesures Techniques et Organisationnelles** : L'application met en œuvre des mesures appropriées pour protéger les données personnelles contre la destruction, la perte, l'altération, la divulgation non autorisée ou l'accès non autorisé.
- **Import/Export Sécurisés** : Les fichiers CSV contenant des données personnelles sont manipulés de manière sécurisée pour éviter tout accès non autorisé.

### Utilisation de Services Externes et CDNs

**Réparticlasse** utilise plusieurs services externes et CDNs pour fonctionner efficacement :

- **Dicebear API** : Génération d'avatars personnalisés.
  - **Données Transmises** :
    - `avatarStyle` : Non personnel, spécifie le style de l'avatar.
    - `avatarSeed` : Généré aléatoirement pour garantir l'anonymat des données.
  - **Conformité RGPD** : Dicebear doit respecter le RGPD. Veuillez consulter leur [Politique de Confidentialité](https://dicebear.com/privacy) pour plus d'informations.

- **CDNs Utilisés** :
  - **Tailwind CSS** : Chargement des styles via [jsdelivr](https://cdn.jsdelivr.net/npm/tailwindcss@2.2.19/dist/tailwind.min.css).
  - **Font Awesome** : Chargement des icônes via [cdnjs.cloudflare.com](https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0-beta3/css/all.min.css).
  - **Intro.js** : Chargement des scripts et styles via [unpkg.com](https://unpkg.com/intro.js/minified/intro.min.js).

**Implications RGPD** :

- **Collecte de Données Techniques** : L'utilisation de ces CDNs implique la transmission d'adresses IP, d'informations sur le navigateur et le système d'exploitation aux serveurs des CDNs.
- **Conformité** : Il est recommandé de vérifier que ces services respectent les exigences du RGPD, notamment en matière de transfert de données hors de l'UE et de garanties contractuelles.

### Transferts de Données Hors de l'UE

Certaines données peuvent être transférées vers des services situés en dehors de l'Union Européenne (UE). **Réparticlasse** s'assure que ces transferts sont conformes au RGPD en mettant en place des garanties appropriées telles que les clauses contractuelles types.

### Responsabilité du Traitement et Sous-Traitance

- **Responsable de Traitement** : L'entité propriétaire de l'application **Réparticlasse** est le Responsable de Traitement des données personnelles.
- **Sous-Traitants** : Les services externes utilisés (Dicebear, CDNs) agissent en tant que Sous-Traitants. Des Data Processing Agreements (DPA) sont mis en place pour garantir leur conformité au RGPD.

### Respect de la LCEN de 2004

**Réparticlasse** respecte les obligations de la LCEN de 2004, notamment en fournissant des mentions légales complètes :

- **Identification de l'Éditeur** : S. Henriot
- **Hébergement** : La Forge



## Prérequis

- Un navigateur web moderne (Chrome, Firefox, Edge).
- Aucune installation nécessaire, l'application est directement utilisable via un navigateur.

## Licence

Ce projet est sous licence Creative Commons Attribution-NonCommercial 4.0 International (CC BY-NC 4.0). Cela signifie que vous êtes libre de partager et d'adapter le matériel pour des utilisations non commerciales, tant que vous créditez l'œuvre et que vous n'appliquez pas de restrictions supplémentaires. Voir le fichier [LICENSE](LICENSE) pour plus de détails.

---

## Ressources et Références

- **Règlement Général sur la Protection des Données (RGPD)** : [Texte officiel](https://eur-lex.europa.eu/legal-content/FR/TXT/?uri=CELEX%3A32016R0679)
- **Guidelines de la CNIL** : [CNIL - RGPD](https://www.cnil.fr/fr/reglement-europeen-protection-donnees)
- **Introduction à Intro.js** : [Documentation](https://introjs.com/docs/)
- **Dicebear Privacy Policy** : [Lien](https://dicebear.com/privacy) *(Vérifier le lien réel)*
- **LCEN 2004** : [Loi n°2004-575 du 21 juin 2004 pour la confiance dans l'économie numérique](https://www.legifrance.gouv.fr/loda/id/JORFTEXT000000801164/)
- **CNIL - Mentions Légales et Obligations** : [CNIL - Mentions Légales](https://www.cnil.fr/fr/modele-de-mentions-legales-pour-un-site-internet)

---

## Contact

Pour toute question relative à la protection des données personnelles ou à la conformité RGPD et LCEN de l'application **Réparticlasse**, veuillez nous contacter par le biais d'un ticket sur la Forge

